<?php

class Pagos extends BaseModel
{
    public static $KEY = 'codigopago';
    public static $TABLE = 'pagos';
    /*************************************/
    public $codigopago;
    public $importe;
    public $fecha; //YYYY-MM-DD
    /*************************************/
    public function __construct($fakeData = false)
    {
        parent::__construct();
		if($fakeData){
			$this->codigopago = (int)rand(18,55);
			$this->importe =  rand(50,809);
		}
    }
    /*************************************/
    public function customValidation()
    {
        if (!($this->importe>0)) {
            throw new Exception('El importe del pago debe ser mayor a 0.');
        }


		if(empty($this->fecha)){
			throw new Exception('El formato de la fecha debe ser YYYY-MM-DD');
		}
		try {
			$d1 = new DateTime($this->fecha);
		} catch (Exception $e) {
			throw new Exception('El formato de la fecha debe ser YYYY-MM-DD');
		}
		$d2 = new DateTime(date('Y-M-d'));
		$diff = (int)$d2->diff($d1)->format('%R%a');
        if ($diff<0) {
            throw new Exception('La fecha de pago no puede ser anterior al día de hoy');
        }
    }
	public function preSave(){
		$this->customValidation();
		parent::preSave();
	}
	public function preUpdate(){
		$this->customValidation();
		parent::preUpdate();
	}
}
