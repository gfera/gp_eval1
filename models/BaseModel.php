<?php

class BaseModel
{
    public static $DB;
    public static $KEY = '';
    public static $TABLE = '';

    /*-----------------------------------*/
    public function __construct()
    {
    }
    /*-----------------------------------*/
    public function preSave()
    {
    }
    public function save()
    {
        $c = get_called_class();
        $this->preSave();
        $this->{$c::$KEY} = rand(1, 999);

        return true;
    }
    /*-------------------------------------*/
    public function preUpdate()
    {
        $c = get_called_class();
        if ($c::$KEY != '' && empty($this->{$c::$KEY})) {
            throw new Exception($c::$TABLE.' debe tener una PK ('.$c::$KEY.') valida para crear un nuevo registro');
        }
    }
    public function update()
    {
        $this->preUpdate();
        return true;
    }
    /*-------------------------------------*/
    public function preDelete()
    {
        $c = get_called_class();
        if ($c::$KEY != '' && empty($this->{$c::$KEY})) {
            throw new Exception($c::$TABLE.' debe tener una PK ('.$c::$KEY.') valida para borrar un registro');
        }
    }
    public function delete()
    {
        $this->preDelete();

        return true;
    }
    /*-------------------------------------*/
    /*-------------------------------------*/
    /*-------------------------------------*/
    /*-------------------------------------*/
    public static function getDB()
    {
        $c = get_called_class();
        if (!$c::$DB) {
            $c::$DB = new ModelQuery();
        }

        return $c::$DB;
    }
    public static function getAll()
    {
        $c = get_called_class();
        $db = $c::getDB();

        return $db->model($c)->result();
    }
    public static function getFirst($condition = '')
    {
        $c = get_called_class();
        $db = $c::getDB();

        return $db->model($c)->where($condition)->first();
    }
    public static function getLast($condition = '')
    {
        $c = get_called_class();
        $db = $c::getDB();

        return $db->model($c)->where($condition)->first();
    }
    public static function getWhere($sql)
    {
        $c = get_called_class();
        $db = $c::getDB();

        return $db->model($c)->where($sql)->result();
    }
}
