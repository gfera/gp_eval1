<?php

class Favoritos extends BaseModel
{
    public static $KEY = '';
    public static $TABLE = 'favoritos';
    /*************************************/
    public $codigousuario;
    public $codigousuariofavorito;
    /*************************************/
    public function __construct($fakeData = false)
    {
        parent::__construct();
		if($fakeData){
			$this->codigousuario = (int)rand(1,500);
			$this->codigousuariofavorito = (int)rand(501,999);
		}
    }
    /*************************************/
    public function customValidation()
    {
        if (!($this->codigousuario > 0)) {
            throw new Exception('codigousuario debe hacer referencia a un registro de Usuario valido.');
        }
        if (!($this->codigousuariofavorito > 0)) {
            throw new Exception('codigousuariofavorito debe hacer referencia a un registro de Usuario valido.');
        }

        if ($this->codigousuariofavorito == $this->codigousuario) {
            throw new Exception('codigousuariofavorito no puede ser igual a codigousuario');
        }
    }
    public function preSave()
    {
        $this->customValidation();
        parent::preSave();
    }
    public function preUpdate()
    {
        $this->customValidation();
        parent::preUpdate();
    }
    public function preDelete()
    {
        if (!($this->codigousuario > 0 && $this->codigousuariofavorito > 0)) {
            throw new Exception('Es necesario tener codigousuario y codigousuariofavorito válidos para borrar el registro');
        }
    }
}
