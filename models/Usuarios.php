<?php

class Usuarios extends BaseModel
{
    public static $KEY = 'codigousuario';
    public static $TABLE = 'usuarios';
    /*************************************/
    public $codigousuario;
    public $usuario;
    public $clave;
    public $edad;
    /*************************************/
    public function __construct($fakeData = null)
    {
        parent::__construct();
		if($fakeData){
			$this->codigousuario = (int)rand(1,999);
			$this->usuario = array("Carlos","Juan","Matías","Leandro","Pablo")[(int)rand(0,4)];
			$this->clave = "saraza";
			$this->edad = (int)rand(18,60);
		}
    }
    /*************************************/
    public function customValidation()
    {
        if (empty($this->usuario)) {
            throw new Exception('El usuario debe tener un nombre valido.');
        }
        if (!($this->edad >= 18)) {
            throw new Exception('El usuario debe ser mayor a 18 años.');
        }
    }
    public function preSave()
    {
        $this->customValidation();
        parent::preSave();
    }
    public function preUpdate()
    {
        $this->customValidation();
        parent::preUpdate();
    }
}
