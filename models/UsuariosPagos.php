<?php

class UsuariosPagos extends BaseModel
{
    public static $KEY = '';
    public static $TABLE = 'usuariospagos';
    /*************************************/
    public $codigousuario;
    public $codigopago;
    /*************************************/
    public function __construct($fakeData = false)
    {
        parent::__construct();
        if ($fakeData) {
            $this->codigousuario = (int) rand(1, 999);
            $this->codigopago = (int) rand(1, 999);
        }
    }
    /*************************************/
    public function customValidation()
    {
        if (!($this->codigousuario > 0)) {
            throw new Exception('codigousuario debe hacer referencia a un registro de Usuario valido.');
        }
        if (!($this->codigopago > 0)) {
            throw new Exception('codigopago debe hacer referencia a un registro de Pago valido.');
        }
    }
    public function preSave()
    {
        $this->customValidation();
        parent::preSave();
    }
    public function preUpdate()
    {
        $this->customValidation();
        parent::preUpdate();
    }
	public function preDelete()
    {
        if (!($this->codigousuario > 0 && $this->codigopago > 0)) {
            throw new Exception('Es necesario tener codigousuario y codigopago válidos para borrar el registro');
        }
    }
}
