<?php


class UsuarioTest{
	public static function nuevoFail(){
		$u = new Usuarios();
		// $u->usuario = "Juan Carlos";
		$u->save();
	}

	public static function nuevoOk(){
		$u = new Usuarios(true);
		if($u->save()){
			echo "Nuevo usuario generado $u->codigousuario\n";
		}
	}
	/*---------------------------------------*/
	public static function updateFail(){
		$u = Usuarios::getFirst();
		// $u->usuario = "Juan Carlos";
		$u->update();
	}
	public static function updateOk(){
		$u = Usuarios::getFirst();
		if($u->update()){
			echo "Usuario $p->codigousuario actualizado\n";
		}
	}
	/*---------------------------------------*/
	public static function deleteFail(){
		$u = new Usuarios();
		$u->delete();
	}
	public static function deleteOk(){
		$u = Usuarios::getFirst();
		if($u->delete()){
			echo "Usuario $u->codigousuario borrado\n";
		}
	}
}

class PagosTest{
	public static function nuevoFail(){
		$p = new Pagos();
		// $p->fecha = '2017-04-01';
		// $p->fecha = '2017-01-01';
		// $p->importe = 654;
		if($p->save()){
			echo "Nuevo pago generado $p->codigopago\n";
		}
	}
	public static function nuevoOK(){
		$p = new Pagos(true);
		if($p->save()){
			echo "Nuevo pago generado $p->codigopago\n";
		}
	}
	/*---------------------------------------------*/
	public static function updateFail(){
		$p = Pagos::getFirst();
		//$p->fecha = null;
		$p->fecha = '2017-01-01';
		if($p->save()){
			echo "Pago actualizado $p->codigopago\n";
		}
	}
	public static function updateOK(){
		$p = Pagos::getFirst();
		$p->importe = 888;
		$p->fecha = '2017-04-28';
		if($p->save()){
			echo "Pago actualizado $p->codigopago\n";
		}
	}
	/*---------------------------------------------*/
	public static function deleteFail(){
		$p = new Pagos();
		$p->delete();
	}
	public static function deleteOK(){
		$p = Pagos::getFirst();
		if($p->delete()){
			echo "Pago borrado $p->codigopago\n";
		}
	}
}

class FavoritosTest{
	public static function nuevoFail(){
		$p = new Favoritos();
		$p->fecha = '2017-04-01';
		if($p->save()){
			echo "Nuevo Favorito generado $p->codigousuario $p->codigousuariofavorito\n";
		}
	}
	public static function nuevoOK(){
		$p = new Favoritos(true);
		if($p->save()){
			echo "Nuevo Favorito generado $p->codigousuario $p->codigousuariofavorito\n";
		}
	}
	/*---------------------------------------------*/
	public static function deleteFail(){
		$p = new Favoritos();
		$p->delete();
	}
	public static function deleteOK(){
		$p = Favoritos::getFirst();
		if($p->delete()){
			echo "Favorito borrado $p->codigousuario $p->codigousuariofavorito\n";
		}
	}
}


class UsuariosPagosTest{
	public static function nuevoFail(){
		$p = new UsuariosPagos();
		if($p->save()){
			echo "Nuevo UsuariosPagos generado $p->codigousuario $p->codigopago\n";
		}
	}
	public static function nuevoOK(){
		$p = new UsuariosPagos(true);
		if($p->save()){
			echo "Nuevo UsuariosPagos generado $p->codigousuario $p->codigopago\n";
		}
	}
	/*---------------------------------------------*/
	public static function deleteFail(){
		$p = new UsuariosPagos();
		$p->delete();
	}
	public static function deleteOK(){
		$p = UsuariosPagos::getFirst();
		if($p->delete()){
			echo "UsuariosPagos borrado $p->codigousuario $p->codigousuariofavorito\n";
		}
	}
}
