<?php

/**
 * Description of ModelBase.
 *
 * @author Geru
 */
class ModelQuery
{
	private $model = null;
	/*-------------------------*/
    public function __construct()
    {

    }
	public function model($m){
		$this->model = $m;
		return $this;
	}

    public function order()
    {
        return $this;
    }
    public function limit()
    {
        return $this;
    }

    public function where($cond = "")
    {
		return $this;
    }

    public function first()
    {
		if($this->model){
			return new $this->model(true);
		}
    }

    public function last()
    {
		if($this->model){
			return new $this->model(true);
		}
    }

    public function result()
    {
		if($this->model){
			return array(
				new $this->model(true),
				new $this->model(true),
				new $this->model(true),
				new $this->model(true),
				new $this->model(true)
			);
		}

    }

	/*-------------------------------------------*/

    public function delete()
    {
		return true;
    }

    public function update()
    {
		return true;
    }
}
