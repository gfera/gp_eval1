<?php

include_once "ModelQuery.php";

function modelLoader($path){
	$dir = 'models/';
	if (file_exists($dir . $path . '.php')) {
		require_once($dir . $path . '.php');
		return true;
	}
	return false;
}
spl_autoload_register('modelLoader');
